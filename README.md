# tap-loans

`tap-loans` is a Singer tap for loans.

Built with [Singer SDK](https://gitlab.com/meltano/singer-sdk).

## Usage with Meltano

You can easily run `tap-loans` by itself or in a pipeline using [Meltano](https://www.meltano.com/).

### Installation

1. [Install Meltano](http://meltano.com/docs/getting-started.html#install-meltano):

    ```bash
    pip install meltano
    ```

3. [Create a project](http://meltano.com/docs/getting-started.html#create-your-meltano-project):

    ```bash
    meltano init tap-loans-playground
    cd tap-loans-playground
    ```

2. Add `tap-loans` to `meltano.yml` under `plugins`/`extractors` as a custom plugin:

    ```yaml
    plugins:
      extractors:
      - name: tap-loans
        namespace: tap_loans
        pip_url: git+https://gitlab.com/DouweM/tap-loans.git
        settings:
        - name: loans
          kind: array
    ```

3. Install `tap-loans`:

    ```bash
    meltano install extractor tap-loans
    ```

### Configuration

Add `loans` to the `tap-loans` definition in `meltano.yml` under `config`:

```yaml
plugins:
  extractors:
  - name: tap-loans
    # ...
    config:
      loans:
      - name: Friend
        start_date: "2021-01-01"
        monthly_balances:
        - 10000.00
        - 9000.00
        - 8000.00
        - 7000.00
        - 6000.00
        - 5000.00
        - 4000.00
        - 3000.00
        - 2000.00
        - 1000.00
```

### Usage

#### Extract only

Run `tap-loans` by itself using `meltano invoke`:

```bash
meltano invoke tap-loans
```

#### Extract & Load

Run `tap-loans` in a pipeline with a loader using `meltano elt`:

```bash
# Add target-jsonl loader
meltano add loader target-jsonl

# Create output directory
mkdir output

# Run pipeline
meltano elt tap-loans target-jsonl

# View results
cat output/assets.jsonl
```

You should see output along the following lines:

```json
{"name": "Friend", "value": 8000.0}
```

## Stand-alone usage

### Installation

```bash
pip install git+https://gitlab.com/DouweM/tap-loans.git
```

### Configuration

Create a `config.json` file with content along the following lines:

```json
{
  "loans": [
    {
      "name": "Friend",
      "start_date": "2021-01-01",
      "monthly_balances": [
        10000.0,
        9000.0,
        8000.0,
        7000.0,
        6000.0,
        5000.0,
        4000.0,
        3000.0,
        2000.0,
        1000.0
      ]
    }
  ]
}
```

### Usage

Run `tap-loans` with the `config.json` file created above:

```bash
tap-loans --config config.json
```
