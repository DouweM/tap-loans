"""Stream class for tap-loans."""

import decimal

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from pathlib import Path
from typing import Any, Dict, List, Optional, Iterable

from singer_sdk import Tap, Stream
from singer_sdk.helpers.typing import (
    ArrayType,
    BooleanType,
    DateTimeType,
    IntegerType,
    NumberType,
    ObjectType,
    PropertiesList,
    Property,
    StringType,
)


class TapLoansStream(Stream):
    pass


class AssetsStream(TapLoansStream):
    name = "assets"

    schema = PropertiesList(
        Property("name", StringType, required=True),
        Property("value", NumberType, required=True),
    ).to_dict()
    primary_keys = ["name"]

    def __init__(self, tap: Tap):
        super().__init__(tap=tap, name=None, schema=None)

    @property
    def partitions(self) -> List[dict]:
        return self.config.get("loans", [])

    def get_records(self, partition: dict) -> Iterable[dict]:
        name = partition["name"]
        start_date = datetime.fromisoformat(partition["start_date"]).date()
        monthly_balances = partition["monthly_balances"]

        delta = relativedelta(date.today(), start_date)
        months = delta.years * 12 + delta.months
        try:
            balance = monthly_balances[months]
        except IndexError:
            balance = 0

        yield {"name": name, "value": balance}
